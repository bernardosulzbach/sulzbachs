#include <algorithm>
#include <bitset>
#include <chrono>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

const size_t N = 12;
const size_t ITERATIONS = 1000000;

typedef std::array<std::array<double, N>, N> Matrix;

static void print(const Matrix &matrix) {
  std::cout << std::fixed << std::setprecision(3);
  for (auto it = matrix.begin(); it != matrix.end(); it++) {
    auto destination = std::ostream_iterator<double>(std::cout, " ");
    std::copy(it->begin(), it->end(), destination);
    std::cout << '\n';
  }
}

static void markov_fill(Matrix &matrix) {
  std::random_device device;
  std::mt19937 generator(device());
  std::uniform_real_distribution<> distribution(1, 2);
  for (size_t i = 0; i < N; ++i) {
    double sum = 0.0;
    for (size_t j = 0; j < N; ++j) {
      matrix[i][j] = distribution(generator);
      sum += matrix[i][j];
    }
    for (size_t j = 0; j < N; ++j) {
      matrix[i][j] /= sum;
    }
  }
}

// C takes A * B.
static void matrix_multiply(Matrix &a, Matrix &b, Matrix &c) {
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < N; ++j) {
      c[i][j] = 0.0;
      for (size_t k = 0; k < N; ++k) {
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}

int main() {
  Matrix a;
  Matrix b;
  Matrix c;
  markov_fill(a);
  b = a;
  std::cout << "Starting with: " << '\n';
  print(b);
  const auto begin = std::chrono::high_resolution_clock::now();
  for (size_t iter = 0; iter < ITERATIONS; ++iter) {
    matrix_multiply(a, b, c);
    matrix_multiply(a, c, b);
  }
  const auto end = std::chrono::high_resolution_clock::now();
  std::cout << "Got to: " << '\n';
  print(b);
  auto delta = end - begin;
  auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(delta);
  auto nanoseconds = duration.count();
  std::cout << "Took " << nanoseconds * pow(10, -6) << " ms." << '\n';
  double flop_per_iteration = 2 * (2 * pow(N, 3));
  double flops = flop_per_iteration * ITERATIONS;
  double gigaflops = flops / nanoseconds;
  std::cout << "Ran at " << gigaflops << " GFLOPS." << '\n';
  return 0;
}
